@echo off 

set pathOfPowershellScript=%TEMP%/makeNewProject-bootstrapper.ps1

echo $pathOfTemporaryMakeNewProjectScript=$env:TEMP + "/" + "makeNewProject.bat" > "%pathOfPowershellScript%"
@rem the following line is a hack to work around an ssl error that appeared out of the blue.
echo [Net.ServicePointManager]::SecurityProtocol=[Net.SecurityProtocolType]::Tls12 >> "%pathOfPowershellScript%"
echo Invoke-WebRequest -Uri "https://gitlab.com/neiljackson1984/evans_cad_standard/raw/master/makeNewProject.bat?inline=false" -OutFile $pathOfTemporaryMakeNewProjectScript >> "%pathOfPowershellScript%"
echo Start-Process -NoNewWindow -FilePath $pathOfTemporaryMakeNewProjectScript -Wait  >> "%pathOfPowershellScript%"
echo Remove-Item -path $pathOfTemporaryMakeNewProjectScript >> "%pathOfPowershellScript%"

powershell -File "%pathOfPowershellScript%"
