@echo off
@rem This script expects to be passed a single parameter on the command line: the path of the project folder that is to be created.

IF [%1] == [] (
	echo. 
	echo Please enter the path of the new project folder that is to be created.
	echo EXAMPLE: J:\Evans Cromwell\^(EC-18-099^) fairfield court 
	set /p projectRoot=
) ELSE (
	set projectRoot=%~1
)

@rem set projectRoot=c:\work\test evans (1234)
echo Creating new project at "%projectRoot%".
@rem to do: refuse to proceed unless either the projectRoot dos not exist or exists and is empty.
mkdir "%projectRoot%"
cd /d "%projectRoot%"
git init

@rem make the standard folders
mkdir delivered & echo > delivered\.empty
mkdir received & echo > received\.empty
mkdir sheets & echo > sheets\.empty
mkdir xref & echo > xref\.empty

@rem this FOR loop is a workaround for not being able to use the variable modifiers on regular variables - it helps us extract the trailing part of the path.
FOR %%G IN ("%projectRoot%") DO set projectName=%%~nxG

REM echo projectName: %projectName%
REM pause

@rem create an empty file simply to allow the first commit to be able to happen, because braid will not operate on a repository that has no commits.
echo > .empty 
git add *
git commit -am "initial commit"
cmd /c braid add https://github.com/neiljackson1984/autocad_block_library_utility    braids/autocad_block_library_utility   
cmd /c braid add https://gitlab.com/neiljackson1984/evans_cad_standard              braids/evans_cad_standard



rem windows seems to lack a standard command (xcopy,perhaps, robocopy?) to copy a folder recursively, so I will fall back on copying every file in the sheets folder.
copy braids\evans_cad_standard\templates\sheets\*                    sheets\                                   1>nul 
copy braids\evans_cad_standard\templates\acaddoc-root.lsp            acaddoc.lsp                               1>nul 
copy braids\evans_cad_standard\templates\acaddoc-subfolder.lsp       xref\acaddoc.lsp                          1>nul 
copy braids\evans_cad_standard\templates\acaddoc-subfolder.lsp       sheets\acaddoc.lsp                        1>nul 
copy braids\evans_cad_standard\templates\main_sheet_set.dst          %projectName%.dst                         1>nul 
@REM copy braids\evans_cad_standard\templates\title_block.dwg         xref\title_block.dwg                      1>nul
copy braids\evans_cad_standard\templates\aerial_plan.png             xref\aerial_plan.png                      1>nul
@REM copy braids\evans_cad_standard\templates\site_plan.jpg           xref\site_plan.jpg                        1>nul
copy braids\evans_cad_standard\templates\vicinity_map.png            xref\vicinity_map.png                     1>nul
copy braids\evans_cad_standard\templates\getImagesFromGoogleMaps.sh  xref\getImagesFromGoogleMaps.sh           1>nul
copy braids\evans_cad_standard\templates\.gitignore                  .gitignore                                1>nul
copy braids\evans_cad_standard\templates\make_deliverable_pdf.bat    make_deliverable_pdf.bat                  1>nul


del /F /Q .empty
del /F /Q sheets\.empty
del /F /Q xref\.empty

git add *
git commit -am "project initialized by makeNewProject.bat"

echo. 
echo. 
echo Finished.
echo Now, please manually edit %projectRoot%\main_sheet_set.dst to customize for the current project.
echo.
goto eof




:eof
pause