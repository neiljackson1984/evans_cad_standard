;; makes a new layout view in the current layout, assigns a newly-generated guid as a name.
(defun C:viewAdd ( / 
    )
    ; (vlax-for view (vla-get-Views (vla-get-ActiveDocument (vlax-get-acad-object)))
        ; (vlax-dump-object view)
    ; )
    
    ; (setq newView 
        ; (vla-Add 
            ; (vla-get-Views (vla-get-ActiveDocument (vlax-get-acad-object)))
            ; (GUID )
            ; ; (strcat "made while on layout " (vl-princ-to-string (vla-get-ObjectID (vla-get-ActiveLayout (vla-get-ActiveDocument (vlax-get-acad-object))))))
        ; )
    ; )
    ; (princ "\n")
    ; (princ "active layout: ") (princ  (vla-get-ObjectID (vla-get-ActiveLayout (vla-get-ActiveDocument (vlax-get-acad-object))))) (princ "\n")
    ; ; (princ "nome of new view: ") (princ  (vla-get-Name newView)) (princ "\n")
    ; (princ "LayoutID of view: ") (princ  (vla-get-LayoutID newView)) (princ "\n")
    (command "-view" "Save" (GUID))
    
)

;; thanks to https://www.theswamp.org/index.php?topic=41820.0
(defun GUID  (/ tl g)
  (if (setq tl (vlax-get-or-create-object "Scriptlet.TypeLib"))
    (progn (setq g (vlax-get tl 'Guid)) (vlax-release-object tl) (substr g 2 36)))
)

