; prompts to select a pviewport, then prompts for the name of a sheet view to set as the value of that pviewport's SheetView property.
(defun C:pviewportAssociateWithSheetView ( /
    pViewport
    pickedEntity
    pickedPoint
    viewportPickingPrompt
    nameOfSheetView
    sheetView
    activeDocument 
    )
    (setq activeDocument (vla-get-ActiveDocument (vlax-get-acad-object)))
    (setq viewportPickingPrompt "Please select a pViewport.")
    ; (setq sheetViewPickingPrompt "Enter the name of a sheet view (or leave blank to create a new sheet view)")
    (setq sheetViewPickingPrompt "Enter the name of a sheet view")
    (princ "\n")
   
    (setq pViewport (vlax-ename->vla-object (car (entsel viewportPickingPrompt))))
    (princ "\n")
    ;;TO-DO validate the selected entity to make sure it is a viewport

    (setq nameOfSheetView 
        (vla-GetString
            (vla-get-Utility activeDocument)
            1 ;; hasSpaces. 1 means that the input can have spaces
               sheetViewPickingPrompt
        )
    )
    

    
    ;;To-Do: validate the sheetView name input that we just retrieved
    ;;To-Do implement creating new sheet view if the nameOfSheetView is empty.
    (setq sheetView 
        (vla-item
            (vla-get-Views activeDocument)
            nameOfSheetView
        )
    )
    
    
    
    (vla-put-SheetView pViewport sheetView)
    (vla-put-HasVpAssociation sheetView :vlax-true)
    
    ; (vlax-dump-object sheetView)
    ; (vlax-dump-object pViewport)
    
    
    (princ)
)
(princ)
    ; (vlax-for view 
        ; (vla-get-Views (vla-get-ActiveDocument (vlax-get-acad-object)))
        ; (vlax-dump-object view)
    ; )
    
    
    ; (vlax-for view 
        ; (vla-get-Views (vla-get-ActiveDocument (vlax-get-acad-object)))
        ; (princ (vla-get-Name view))(princ "\n")
    ; )


    ; (vlax-dump-object
        ; (vla-get-SheetView
            ; (vlax-ename->vla-object (car (entsel)))
        ; )
    ; )
        ; (vlax-dump-object  (vlax-ename->vla-object (car (entsel))))
    ; ; 397d9adf58dd4ff4af2e55609ad501f1
    
; (vlax-dump-object (vla-item (vla-get-Views (vla-get-ActiveDocument (vlax-get-acad-object))) "397d9adf58dd4ff4af2e55609ad501f1"))