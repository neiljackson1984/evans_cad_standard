
(progn ;function definitions
    (defun setAttribute (blockReference attributeName newValue
        /
        returnValue
        )
        (setq returnValue nil)
        (foreach attributeReference (gc:VariantToLispData  (vla-GetAttributes blockReference))
            (if (not returnValue)
                (progn
                    (if (= (strcase (vla-get-TagString attributeReference))(strcase attributeName))
                        (progn
                            (vla-put-TextString attributeReference newValue)
                            (setq returnValue newValue)
                        )
                    )
                )
            )
        )

        returnValue
    )

    ;; Variant -> LISP

    ;; gc:VariantToLispData
    ;; Converts a variant or a safearray into LISP data (list)
    ;;
    ;; Argument: var variant or safearray

    (defun gc:VariantToLispData (var)
      (cond
        ((= (type var) 'variant)
         (gc:VariantToLispData (vlax-variant-value var)))
        ((= (type var) 'safearray)
         (mapcar 'gc:VariantToLispData (vlax-safearray->list var))
        )
        (T var)
      )
    )
    
    	;returns the center of the bounding box of the entity
	(defun getMidpoint (entity 
		/
		minPoint
		maxPoint
		midPoint
		)
		(vla-GetBoundingBox entity 'minPoint 'maxPoint)
		(setq minPoint (gc:VariantToLispData minPoint))
		(setq maxPoint (gc:VariantToLispData maxPoint))
		
		
		(setq midPoint
			(mapcar '(lambda (a b) (/ (+ a b) 2)) 
				minPoint
				maxPoint
			)
		)
		midPoint
	)
)





; we will sort from top-to-bottom (i.e. decreasing y), then left to right (i.e. increasing x)
	(setq firstSortingCoordinate 1) (setq firstSortingOperator >) ; decreasing y
	(setq secondSortingCoordinate 0) (setq secondSortingOperator <)   ; increasing x
	
	(setq fuzz 0.6) ; for our sorting algorithm, we will consider two coordinates to be equal if they are within fuzz of one another.
    
(setq blockReferences nil)	
(vlax-for entity (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object)))
    (if
        (and 
            (= (vla-get-ObjectName entity) "AcDbBlockReference")
            (= (vla-get-Name entity) "Fan Filter Unit")
        )
        (setq blockReferences (cons entity blockReferences))
    )
)

	
	(setq blockReferences
		(vl-sort blockReferences
			; sort by layer, then by x coordinate, then by y coordinate, then by handle.
			'(lambda (a b / midpointOfA midpointOfB)
				(setq midpointOfA (getMidpoint a))
				(setq midpointOfB (getMidpoint b))
				
				(if (/= (vla-get-Layer a) (vla-get-Layer b))
					(progn
						(< (vla-get-Layer a) (vla-get-Layer b))
					)
					(progn
						; (if (/= (nth firstSortingCoordinate midpointOfA) (nth firstSortingCoordinate midpointOfB))
						(if (not (equal (nth firstSortingCoordinate midpointOfA) (nth firstSortingCoordinate midpointOfB) fuzz))
							(progn 
								(firstSortingOperator (nth firstSortingCoordinate midpointOfA) (nth firstSortingCoordinate midpointOfB))
							)
							(progn
								; (if (/= (nth secondSortingCoordinate midpointOfA) (nth secondSortingCoordinate midpointOfB))
								(if (not (equal (nth secondSortingCoordinate midpointOfA) (nth secondSortingCoordinate midpointOfB) fuzz))
									(progn 
										(secondSortingOperator (nth secondSortingCoordinate midpointOfA) (nth secondSortingCoordinate midpointOfB))
									)
									(progn
										(< (vla-get-Handle a) (vla-get-Handle b))
									)
								)
							)
						)
					)
				)
			)
		)
	)


(setq i 1)

(foreach blockReference blockReferences
    (setq newValue (strcat "FFU-" (itoa i)))
    (setAttribute blockReference "identifier" newValue)
    (setq i (+ 1 i))
)


; (while T
    ; ;;(princ "i is ")(princ i)(princ "\n")
    ; (setq selectedEntity (vlax-ename->vla-object (car (entsel "select the next block reference"))))
    ; (if 
        ; (and 
            ; (= (vla-get-ObjectName selectedEntity) "AcDbBlockReference")
            ; (= (vla-get-Name selectedEntity) "Fan Filter Unit")
        ; )
        ; (progn
            ; (setq blockReference selectedEntity)
            ; ;; (princ "you clicked a block named ")(princ (vla-get-Name blockReference))(princ "\n")
            ; (setq newValue (strcat "FFU-" (itoa i)))
            ; (setAttribute blockReference "identifier" newValue)
            ; (princ "set the attribute value to " )(princ newValue)(princ "\n")
            
        ; )
        ; (progn
            ; (princ "ignoring the thing you selected because it is not one of the block references weare after.  Please try again.\n")
        ; )
    ; )
; )
