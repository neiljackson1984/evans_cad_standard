; This script changes the name of the text style and multileader style currently named "Standard"

;; thanks to https://www.theswamp.org/index.php?topic=41820.0
(defun GUID  (/ tl g)
  (if (setq tl (vlax-get-or-create-object "Scriptlet.TypeLib"))
    (progn (setq g (vlax-get tl 'Guid)) (vlax-release-object tl) (substr g 2 36)))
)

(princ (GUID))

(princ "attempting to rename mleader stlye: ")
(princ
    (vl-catch-all-apply
        'vla-put-Name 
        (list 
            (vl-catch-all-apply 'vla-item 
                (list 
                    (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_MLEADERSTYLE") 
                    "Standard"
                )
            ) 
            (GUID)
        )
    )
)
(princ "\n")

(princ "attempting to rename table stlye: ")
(princ
    (vl-catch-all-apply
        'vla-put-Name 
        (list 
            (vl-catch-all-apply 'vla-item 
                (list 
                    (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_TABLESTYLE") 
                    "Standard"
                )
            ) 
            (GUID)
        )
    )
)
(princ "\n")

(princ "attempting to rename text stlye: ")
(princ
    (vl-catch-all-apply
        'vla-put-Name 
        (list 
            (vl-catch-all-apply 'vla-item 
                (list 
                   (vla-get-TextStyles (vla-get-ActiveDocument (vlax-get-acad-object)))
                    "Standard"
                )
            ) 
            (GUID)
        )
    )
)
(princ "\n")

(princ "attempting to rename dim stlye: ")
(princ
    (vl-catch-all-apply
        'vla-put-Name 
        (list 
            (vl-catch-all-apply 'vla-item 
                (list 
                   (vla-get-DimStyles (vla-get-ActiveDocument (vlax-get-acad-object)))
                    "Standard"
                )
            ) 
            (GUID)
        )
    )
)
(princ "\n")
; Inexplicably, the Name property of a TextStyle object is read-only.  However, the following command achieves the desired result.
; (vl-catch-all-apply 'command (list "-RENAME" "Style" "Standard" (GUID)) )
(command "-RENAME" "Style" "Standard" (GUID) )



; (setq keys (list ))
; (vlax-for item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object)))
    ; (setq keys
        ; (append
            ; keys
            ; (list (vl-catch-all-apply 'vla-get-Name (list item)))
        ; )
    ; )
; )
; (foreach key keys
    ; (princ key)(princ "\n")
; )
; (princ)


(vla-put-Name 
    (vl-catch-all-apply 'vla-item 
        (list 
           (vla-get-TextStyles (vla-get-ActiveDocument (vlax-get-acad-object)))
            "Standard"
        )
    ) 
    (GUID)
)
