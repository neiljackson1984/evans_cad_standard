
;; generates the list (0 1 2 3 ... (rangeSize - 1))
(defun range (rangeSize /
	i
	returnValue
	)
	(setq returnValue (list))
	(setq i 0)
	(while (< i rangeSize) 
		(setq returnValue (append returnValue (list i)))
		;(setq i (+ 1 i))
		(plusPlus 'i)
	)
	returnValue
)

;; generates the  list (start, start + increment, start + 2 * increment, ..., end)
(defun rangeFromTo (start end increment /
    returnValue
    i
    rangeSize
    )
    (setq rangeSize (/ (- end start) increment))
    (setq rangeSize (+ 1 rangeSize)) ;;the +1 causes us to include the endpoint.
    (setq returnValue (list))
    (setq i 0)
    (while (< i rangeSize) 
		(setq returnValue 
            (append returnValue 
                (list 
                    (+ start (* i increment))
                )
            )
        )
		(setq i (+ 1 i))
	)
    returnValue
)

;; this plusPlus function behaves like the postfix '++' operator in C++ and similar languages.  That is, it returns the initial value of x (the value Before incrementing).
(defun plusPlus (x / initialValue returnValue)
	(if (= (type x) 'SYM)
		(progn
			(setq returnValue (eval x))
			(set x (+ 1 (eval x)))
		)
		(progn
			(setq returnValue x) ;; in the case where x is not a symbol, we will return x.  This is just to be thorough - I only intend to use plusPlus x to operate on symbols.
		)
	)
	returnValue
)


(progn ;; initialize the myTable object
	;; (vlax-dump-object (vlax-ename->vla-object (car (entsel))))
	;; "12465"
	;;(setq myTable (vla-HandleToObject (vla-get-ActiveDocument (vlax-get-acad-object)) "12465"))
	(if (not myTable)
		(progn 
			(setq myTable (vlax-ename->vla-object (car (entsel))))
		)
	)
	;;(vlax-dump-object myTable)
)


(progn ;; define some enum values

	(setq AcCellProperty_enumValues
		(list
			(cons acAlignmentProperty        "acAlignmentProperty")
			(cons acAllCellProperties        "acAllCellProperties")
			(cons acAutoScale                "acAutoScale")
			(cons acBackgroundColor          "acBackgroundColor")
			(cons acBitProperties            "acBitProperties")
			(cons acContentColor             "acContentColor")
			(cons acContentLayout            "acContentLayout")
			(cons acContentProperties        "acContentProperties")
			(cons acDataFormat               "acDataFormat")
			(cons acDataType                 "acDataType")
			(cons acDataTypeAndFormat        "acDataTypeAndFormat")
			(cons acEnableBackgroundColor    "acEnableBackgroundColor")
			(cons acFlowDirBtoT              "acFlowDirBtoT")
			(cons acInvalidCellProperty      "acInvalidCellProperty")
			(cons acLock                     "acLock")
			(cons acMarginBottom             "acMarginBottom")
			(cons acMarginLeft               "acMarginLeft")
			(cons acMarginRight              "acMarginRight")
			(cons acMarginTop                "acMarginTop")
			(cons acMergeAll                 "acMergeAll")
			(cons acRotation                 "acRotation")
			(cons acScale                    "acScale")
			(cons acTextHeight               "acTextHeight")
			(cons acTextStyle                "acTextStyle")
		)
	)

	(setq variantType_enumValues 
		(list
			(cons  vlax-vbEmpty        "vlax-vbEmpty"    )
			(cons  vlax-vbNul          "vlax-vbNul"      )
			(cons  vlax-vbInteger      "vlax-vbInteger"  )
			(cons  vlax-vbLong         "vlax-vbLong"     )
			(cons  vlax-vbSingle       "vlax-vbSingle"   )
			(cons  vlax-vbDouble       "vlax-vbDouble"   )
			(cons  vlax-vbString       "vlax-vbString"   )
			(cons  vlax-vbObject       "vlax-vbObject"   )
			(cons  vlax-vbBoolean      "vlax-vbBoolean"  )
			(cons  vlax-vbArray        "vlax-vbArray"    )

		)
	)
	(setq AcGridLineType_enumValues ;; these appear to be bitmask values
		(list
			(cons acHorzBottom      "acHorzBottom"       )
			(cons acHorzInside      "acHorzInside"       )
			(cons acHorzTop         "acHorzTop"          )
			(cons acInvalidGridLine "acInvalidGridLine"  )
			(cons acVertInside      "acVertInside"       )
			(cons acVertLeft        "acVertLeft"         )
			(cons acVertRight       "acVertRight"        )
		)
	)

	
	(setq AcRowType_enumValues ;; these appear to be bitmask values
		(list
			(cons  acDataRow       "acDataRow"    )
			(cons  acHeaderRow     "acHeaderRow"  )
			(cons  acTitleRow      "acTitleRow"   )
			(cons  acUnknownRow    "acUnknownRow" )
		)
	)
	
	(setq acTableStyleOverrides_enumValues
		(list
			(cons acCellAlign                     "acCellAlign"                     )
			(cons acCellBackgroundColor           "acCellBackgroundColor"           )
			(cons acCellBackgroundFillNone        "acCellBackgroundFillNone"        )
			(cons acCellBottomGridColor           "acCellBottomGridColor"           )
			(cons acCellBottomGridLineWeight      "acCellBottomGridLineWeight"      )
			(cons acCellBottomVisibility          "acCellBottomVisibility"          )
			(cons acCellContentColor              "acCellContentColor"              )
			(cons acCellDataType                  "acCellDataType"                  )
			(cons acCellLeftGridColor             "acCellLeftGridColor"             )
			(cons acCellLeftGridLineWeight        "acCellLeftGridLineWeight"        )
			(cons acCellLeftVisibility            "acCellLeftVisibility"            )
			(cons acCellRightGridColor            "acCellRightGridColor"            )
			(cons acCellRightGridLineWeight       "acCellRightGridLineWeight"       )
			(cons acCellRightVisibility           "acCellRightVisibility"           )
			(cons acCellTextHeight                "acCellTextHeight"                )
			(cons acCellTextStyle                 "acCellTextStyle"                 )
			(cons acCellTopGridColor              "acCellTopGridColor"              )
			(cons acCellTopGridLineWeight         "acCellTopGridLineWeight"         )
			(cons acCellTopVisibility             "acCellTopVisibility"             )
			(cons acDataHorzBottomColor           "acDataHorzBottomColor"           )
			(cons acDataHorzBottomLineWeight      "acDataHorzBottomLineWeight"      )
			(cons acDataHorzBottomVisibility      "acDataHorzBottomVisibility"      )
			(cons acDataHorzInsideColor           "acDataHorzInsideColor"           )
			(cons acDataHorzInsideLineWeight      "acDataHorzInsideLineWeight"      )
			(cons acDataHorzInsideVisibility      "acDataHorzInsideVisibility"      )
			(cons acDataHorzTopColor              "acDataHorzTopColor"              )
			(cons acDataHorzTopLineWeight         "acDataHorzTopLineWeight"         )
			(cons acDataHorzTopVisibility         "acDataHorzTopVisibility"         )
			(cons acDataRowAlignment              "acDataRowAlignment"              )
			(cons acDataRowColor                  "acDataRowColor"                  )
			(cons acDataRowDataType               "acDataRowDataType"               )
			(cons acDataRowFillColor              "acDataRowFillColor"              )
			(cons acDataRowFillNone               "acDataRowFillNone"               )
			(cons acDataRowTextHeight             "acDataRowTextHeight"             )
			(cons acDataRowTextStyle              "acDataRowTextStyle"              )
			(cons acDataVertInsideColor           "acDataVertInsideColor"           )
			(cons acDataVertInsideLineWeight      "acDataVertInsideLineWeight"      )
			(cons acDataVertInsideVisibility      "acDataVertInsideVisibility"      )
			(cons acDataVertLeftColor             "acDataVertLeftColor"             )
			(cons acDataVertLeftLineWeight        "acDataVertLeftLineWeight"        )
			(cons acDataVertLeftVisibility        "acDataVertLeftVisibility"        )
			(cons acDataVertRightColor            "acDataVertRightColor"            )
			(cons acDataVertRightLineWeight       "acDataVertRightLineWeight"       )
			(cons acDataVertRightVisibility       "acDataVertRightVisibility"       )
			(cons acFlowDirection                 "acFlowDirection"                 )
			(cons acHeaderHorzBottomColor         "acHeaderHorzBottomColor"         )
			(cons acHeaderHorzBottomLineWeight    "acHeaderHorzBottomLineWeight"    )
			(cons acHeaderHorzBottomVisibility    "acHeaderHorzBottomVisibility"    )
			(cons acHeaderHorzInsideColor         "acHeaderHorzInsideColor"         )
			(cons acHeaderHorzInsideLineWeight    "acHeaderHorzInsideLineWeight"    )
			(cons acHeaderHorzInsideVisibility    "acHeaderHorzInsideVisibility"    )
			(cons acHeaderHorzTopColor            "acHeaderHorzTopColor"            )
			(cons acHeaderHorzTopLineWeight       "acHeaderHorzTopLineWeight"       )
			(cons acHeaderHorzTopVisibility       "acHeaderHorzTopVisibility"       )
			(cons acHeaderRowAlignment            "acHeaderRowAlignment"            )
			(cons acHeaderRowColor                "acHeaderRowColor"                )
			(cons acHeaderRowDataType             "acHeaderRowDataType"             )
			(cons acHeaderRowFillColor            "acHeaderRowFillColor"            )
			(cons acHeaderRowFillNone             "acHeaderRowFillNone"             )
			(cons acHeaderRowTextHeight           "acHeaderRowTextHeight"           )
			(cons acHeaderRowTextStyle            "acHeaderRowTextStyle"            )
			(cons acHeaderSuppressed              "acHeaderSuppressed"              )
			(cons acHeaderVertInsideColor         "acHeaderVertInsideColor"         )
			(cons acHeaderVertInsideLineWeight    "acHeaderVertInsideLineWeight"    )
			(cons acHeaderVertInsideVisibility    "acHeaderVertInsideVisibility"    )
			(cons acHeaderVertLeftColor           "acHeaderVertLeftColor"           )
			(cons acHeaderVertLeftLineWeight      "acHeaderVertLeftLineWeight"      )
			(cons acHeaderVertLeftVisibility      "acHeaderVertLeftVisibility"      )
			(cons acHeaderVertRightColor          "acHeaderVertRightColor"          )
			(cons acHeaderVertRightLineWeight     "acHeaderVertRightLineWeight"     )
			(cons acHeaderVertRightVisibility     "acHeaderVertRightVisibility"     )
			(cons acHorzCellMargin                "acHorzCellMargin"                )
			(cons acTitleHorzBottomColor          "acTitleHorzBottomColor"          )
			(cons acTitleHorzBottomLineWeight     "acTitleHorzBottomLineWeight"     )
			(cons acTitleHorzBottomVisibility     "acTitleHorzBottomVisibility"     )
			(cons acTitleHorzInsideColor          "acTitleHorzInsideColor"          )
			(cons acTitleHorzInsideLineWeight     "acTitleHorzInsideLineWeight"     )
			(cons acTitleHorzInsideVisibility     "acTitleHorzInsideVisibility"     )
			(cons acTitleHorzTopColor             "acTitleHorzTopColor"             )
			(cons acTitleHorzTopLineWeight        "acTitleHorzTopLineWeight"        )
			(cons acTitleHorzTopVisibility        "acTitleHorzTopVisibility"        )
			(cons acTitleRowAlignment             "acTitleRowAlignment"             )
			(cons acTitleRowColor                 "acTitleRowColor"                 )
			(cons acTitleRowDataType              "acTitleRowDataType"              )
			(cons acTitleRowFillColor             "acTitleRowFillColor"             )
			(cons acTitleRowFillNone              "acTitleRowFillNone"              )
			(cons acTitleRowTextHeight            "acTitleRowTextHeight"            )
			(cons acTitleRowTextStyle             "acTitleRowTextStyle"             )
			(cons acTitleSuppressed               "acTitleSuppressed"               )
			(cons acTitleVertInsideColor          "acTitleVertInsideColor"          )
			(cons acTitleVertInsideLineWeight     "acTitleVertInsideLineWeight"     )
			(cons acTitleVertInsideVisibility     "acTitleVertInsideVisibility"     )
			(cons acTitleVertLeftColor            "acTitleVertLeftColor"            )
			(cons acTitleVertLeftLineWeight       "acTitleVertLeftLineWeight"       )
			(cons acTitleVertLeftVisibility       "acTitleVertLeftVisibility"       )
			(cons acTitleVertRightColor           "acTitleVertRightColor"           )
			(cons acTitleVertRightLineWeight      "acTitleVertRightLineWeight"      )
			(cons acTitleVertRightVisibility      "acTitleVertRightVisibility"      )
			(cons acVertCellMargin                "acVertCellMargin"                )
		)                                                                           
	)
	(setq AcCellContentLayout_enumValues
		(list
			(cons acCellContentLayoutFlow                  "acCellContentLayoutFlow"               )
			(cons acCellContentLayoutStackedHorizontal     "acCellContentLayoutStackedHorizontal"  )
			(cons acCellContentLayoutStackedVertical       "acCellContentLayoutStackedVertical"    )
		)
	)
	
	(setq AcCellContentType_enumValues
		(list
			(cons acCellContentTypeBlock     "acCellContentTypeBlock"      )
			(cons acCellContentTypeField     "acCellContentTypeField"      )
			(cons acCellContentTypeUnknown   "acCellContentTypeUnknown"    )
			(cons acCellContentTypeValue     "acCellContentTypeValue"      )
		)
	)
)



(progn ;; report on the table:
	(vlax-dump-object myTable)
	
	(foreach rowIndex (range (vla-get-rows myTable))
		(princ "row ") (princ rowIndex)(princ ":")(princ "\n")
		(princ "\t")(princ "(vla-GetRowType myTable rowIndex): ")(princ (cdr (assoc (vla-GetRowType myTable rowIndex) AcRowType_enumValues)))(princ "\n")
		(princ "\t")(princ "(vla-GetRowHeight myTable rowIndex): ")(princ (vla-GetRowHeight myTable rowIndex))(princ "\n")

		(princ "\t")(princ "cells: ")(princ "\n")
		(foreach columnIndex (range (vla-get-columns myTable))
			(princ "\t\t")(princ "cell ")(princ "(")(princ rowIndex)(princ " ")(princ columnIndex)(princ ")")(princ ": ")                                                                               (princ "\n")


			;;figure out how many content items this table cell contains (content items are indexed starting from zero.  By default, every cell has zero content items.  The stuff that the user inserts into a cell with the UI tends to end up in content item zero.
			(setq maxContentIndexToCheck 100) ;; a guard to prevent an endless loop, in case the function does not ever throw an exception.
			;; step through contentIndices and call some function that needs a valid contentIndex until we hit an exception.
			(setq contentIndex 0)
			(setq numberOfContentItems 0)
			(while 
				(and 
					(< contentIndex maxContentIndexToCheck)
					(or
						; (not 
							; (vl-catch-all-error-p 
								; (setq result 
									; (vl-catch-all-apply 
										; ; 'vla-GetBlockTableRecordId2 (list myTable rowIndex columnIndex contentIndex) ;;returns zero without throwing exception on non-existent content items.
										; 'vla-GetValue (list myTable rowIndex columnIndex contentIndex)
									; )
								; )
							; )
						; ) 
						(/= (vlax-variant-type (vla-GetValue myTable rowIndex columnIndex contentIndex)) vlax-vbEmpty) ; vla-GetValue will return an empty variant in the case where the content contains a block reference.
						
						;; this is not too menaningful in testing for the existence of a content item because GetDataType2 says that the data type is acGeneral in the case where the content item does not exist.
						; (/=
							; (progn	
								; (vla-GetDataType2 myTable rowIndex columnIndex contentIndex 'dataType_out 'unitType_out)
								; dataType_out
							; )
							; acUnknownDataType
						; )
						
						(/= 
							(vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex)
							0
						)
						
					)
				)
				
				; (princ "result: ")
				; (princ
					; (cdr 
						; (assoc (vlax-variant-type result) variantType_enumValues )
					; )
				; )
				; (princ result) 
				; (princ "\n")
				(plusPlus 'contentIndex)
				(plusPlus 'numberOfContentItems)
			)
			
			
			(princ "\t\t\t")(princ     "ContentLayout: "       ) (princ "\t") (princ (cdr (assoc (vla-GetContentLayout myTable rowIndex columnIndex) AcCellContentLayout_enumValues)))  (princ "\n")
			(princ "\t\t\t")(princ     "ContentType: "         ) (princ "\t") (princ (cdr (assoc (vla-GetContentType myTable rowIndex columnIndex) AcCellContentType_enumValues)))      (princ "\n")
			(princ "\t\t\t")(princ     "CellStyle: "           ) (princ "\t") (princ (vla-GetCellStyle myTable rowIndex columnIndex))                                                   (princ "\n")
			(princ "\t\t\t")(princ     "CellStyleOverrides: "  )(princ "\t")                                                 
			(princ 
				(mapcar	
					'(lambda (x) 
						; (list
							; x
							; (cdr (assoc x acTableStyleOverrides_enumValues))
						; )
						(cdr (assoc x acTableStyleOverrides_enumValues))
					)
					; (if 
						; (vl-catch-all-error-p  
							; (setq result 
								; (vl-catch-all-apply 
									; 'vla-GetCellStyleOverrides (list myTable rowIndex columnIndex)
								; )
							; )
						; )
						; (list )
						(if 
							(vl-catch-all-error-p  
								(setq result 
									(vl-catch-all-apply 
										'gc:VariantToLispData (list  (vla-GetCellStyleOverrides myTable rowIndex columnIndex))
									)
								)
							)
							(list )
							result
						)
					; )
				)
			)
			(princ "\n")
			
			
			(princ "\t\t\t")(princ "has ")(princ numberOfContentItems)(princ " content items:")(princ "\n")
			(foreach contentIndex (range numberOfContentItems)
				
				(setq content (vla-GetValue  myTable rowIndex columnIndex contentIndex))
				(setq contentValue 
					(vl-catch-all-apply 'vlax-variant-value (list content))
				) 
				(princ "\t\t\t\t")(princ "content ")(princ contentIndex)(princ ": ")
				(princ
					(cdr 
						(assoc 
							(vlax-variant-type content)
							(list
								(cons  vlax-vbEmpty        "vlax-vbEmpty"    )
								(cons  vlax-vbNul          "vlax-vbNul"      )
								(cons  vlax-vbInteger      "vlax-vbInteger"  )
								(cons  vlax-vbLong         "vlax-vbLong"     )
								(cons  vlax-vbSingle       "vlax-vbSingle"   )
								(cons  vlax-vbDouble       "vlax-vbDouble"   )
								(cons  vlax-vbString       "vlax-vbString"   )
								(cons  vlax-vbObject       "vlax-vbObject"   )
								(cons  vlax-vbBoolean      "vlax-vbBoolean"  )
								(cons  vlax-vbArray        "vlax-vbArray"    )
							)
						)
					)
				)
				(princ " ")
				(princ
					(cdr 
						(assoc 
							(progn	
								(vla-GetDataType2 myTable rowIndex columnIndex contentIndex 'dataType_out 'unitType_out)
								dataType_out
							)
							(list
								(cons  acBuffer          "acBuffer"            )
								(cons  acDate            "acDate"              )
								(cons  acDouble          "acDouble"            )
								(cons  acGeneral         "acGeneral"           )
								(cons  acLong            "acLong"              )
								(cons  acObjectId        "acObjectId"          )
								(cons  acPoint2d         "acPoint2d"           )
								(cons  acPoint3d         "acPoint3d"           )
								(cons  acResbuf          "acResbuf"            )
								(cons  acString          "acString"            )
								(cons  acUnknownDataType "acUnknownDataType"   )
							)
						)
					)
				)
				(princ " ")
				(if (/= 0 (vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex))
					(progn
						(setq blockDefinition (vla-ObjectIDToObject (vla-get-Document myTable) (vla-GetBlockTableRecordId2 myTable rowIndex columnIndex contentIndex)))
						(princ (vla-get-ObjectName blockDefinition))(princ ": ")
						(princ (vla-get-Name blockDefinition))
						;;(vlax-dump-object blockDefinition)
					)
					(progn	
						(princ "content: ")(princ contentValue)
					)
				)
				
				(princ "\n")
				;(vla-GetBlockTableRecordId2 myTable rowIndex columnIndex)
			)
			
			
			; (setq contentId (vla-CreateContent myTable rowIndex columnIndex -1))
			; (princ "contentId: ")(princ contentId)
			; (vla-setTextString myTable 
				; rowIndex
				; columnIndex
				; contentId
				; (strcat "xxx" (itoa contentId))
			; )
			
			;(vla-DeleteContent myTable rowIndex columnIndex contentId)
			
			; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 3))
			; (vla-setTextString reportTable 
				; rowIndex
				; columnIndex
				; 1
				; (strcat "xxx" (itoa contentId))
			; )
			; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
			; (vla-setTextString reportTable 
				; rowIndex
				; columnIndex
				; contentId
				; (strcat "xxx" (itoa contentId))
			; )
			
			; (setq contentId (vla-CreateContent reportTable rowIndex columnIndex 16))
			; (vla-setTextString reportTable 
				; rowIndex
				; columnIndex
				; contentId
				; (strcat "GetCellBackgroundColorNone" (if (= :vlax-true (vla-GetCellBackgroundColorNone reportTable rowIndex columnIndex)) ":vlax-true" ":vlax-false"))
			; )
			
			
			
		)
	)



)














;; sorts a range of table cells
(setq locationsToSort 
    (list
        (rangeFromTo 28 119 1) ;;row indices (zero-based)
        (rangeFromTo 0 1 1) ;;column indices (zero-based)
    )
)

;; extract the sortableData
(setq sortableData (list))
(foreach rowIndex (nth 0 locationsToSort)
    (setq thisRowOfData (list))
    
    (foreach columnIndex (nth 1 locationsToSort)
        (setq thisCellValue 
            (vl-catch-all-apply 'vlax-variant-value 
                (list 
                    (vla-GetValue  myTable 
                        rowIndex ;; rowIndex 
                        columnIndex ;; columnIndex 
                        0 ;; contentIndex  for now, we will only look at the first content item.
                    )                
                )
            )
        )
        
        (setq thisRowOfData 
            (append
                thisRowOfData
                (list
                    thisCellValue
                )
            )
        )
    )
    
    (setq sortableData 
        (append
            sortableData
            (list
                thisRowOfData
            )
        )
    )
)

; (princ "checkpoint\n")
; (princ sortableData)

;;sort the sortable data
(setq sortedData
    (vl-sort
        sortableData
        '(lambda (a b)
            (<
                (if (not (nth 0 a)) "" (strcase (nth 0 a) ))
                (if (not (nth 0 b)) "" (strcase (nth 0 b) ))
            )
        )
    )
)

;;write sorted data back into the table
(foreach rowIndexIndex (range (length (nth 0 locationsToSort)))
    (foreach columnIndexIndex (range (length (nth 1 locationsToSort)))
        (setq cellValue 
            (nth    
                columnIndexIndex
                (nth rowIndexIndex sortedData)
            )
        )
        (setq rowIndex (nth rowIndexIndex (nth 0 locationsToSort)))
        (setq columnIndex (nth columnIndexIndex (nth 1 locationsToSort)))
        (vla-SetValue  myTable 
            rowIndex     ;; rowIndex 
            columnIndex  ;; columnIndex 
            0            ;; contentIndex 
            (vlax-make-variant cellValue)
        )
    )
)