(vlax-for entity 
    (vla-Item (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "cutsheet")
    ; (vlax-dump-object entity)
    (vla-put-EntityTransparency entity 0)
    (setq color 
        (vla-get-TrueColor entity)
    )
    (vla-put-ColorIndex color acWhite)
    
    (vla-put-TrueColor entity color)
    (vlax-dump-object entity)
)