(setq tableStyle
    (vla-item 
        (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_TABLESTYLE")
        ; (vla-get-StyleName myTable)
        "construction_notes"
    )
)


(setq tableTemplate
    (vla-ObjectIdToObject 
        (vla-get-Document tableStyle)
        (vla-get-TemplateId tableStyle)
    )
)

(vlax-dump-object tableStyle)
(vlax-dump-object tableTemplate)
(princ "(vla-get-rows tableStyle): ")(princ (vl-catch-all-apply 'vla-get-rows (list tableStyle)))(princ "\n")
(princ "(vla-get-rows tableTemplate): ")(princ (vl-catch-all-apply 'vla-get-rows (list tableTemplate)))(princ "\n")

(vlax-for dictionary (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object)))
    (princ "name of dictinary: ")(princ (vl-catch-all-apply 'vla-get-Name (list dictionary)))(princ "\n")
)

(princ)
