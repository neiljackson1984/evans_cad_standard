(vlax-dump-object
    (vla-get-ModelSpace
        (vla-get-ActiveDocument
            (vlax-get-acad-object)
        )
    )
)

(princ "entities in model space: ")(princ "\n")
(vlax-for entity 
    (vla-get-ModelSpace
        (vla-get-ActiveDocument
            (vlax-get-acad-object)
        )
    )
    (princ (vla-get-ObjectName entity))(princ "\n")
)