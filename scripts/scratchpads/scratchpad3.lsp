(setq blockReferences (list ))

(setq needleName "_ClosedBlank") ; this is the name of the block definition whose references we are searching for.
; (setq needleName "vma600gvju-installationDrawing") ; this is the name of the block definition whose references we are searching for.

(vlax-for blockDefinition 
	(vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))


		
			; (princ "now looking at blockDefinition ")(princ (vla-get-Name blockDefinition))(princ "\n")
			(vlax-for entity blockDefinition
				(if 
					(= (vla-get-ObjectName entity) "AcDbBlockReference" )
					(progn
						; (princ "found a blockReference within ")(princ (vla-get-Name blockDefinition))(princ "\n")
						(if
							(vl-catch-all-error-p 
								(setq nameOfBlockReference 
									(vl-catch-all-apply 
										'vla-get-Name (list entity)
									) 
								)
							)
							(progn
								(princ "trying to get the name of the block reference through an exception: ")(princ (vl-catch-all-error-message nameOfBlockReference))(princ "\n")
								(princ "\t")(princ "name of blockDefinition containing the nameless blockReference: ")(princ (vla-get-Name blockDefinition))(princ "\n")
								(vla-delete entity)
							)
							(progn
								(if 
									(= (vla-get-Name entity)   needleName)
									(progn
										(princ "found a reference ")(princ "(handle: ")(princ (vla-get-Handle entity))(princ ") to (the block definition named \"") (princ needleName) (princ "\") within the block definition named ")(princ (vla-get-Name blockDefinition))(princ "\n")
										(appendTo 'blockReferences entity)
									)
								)
							)
						)
					)
				)
			)
		

)

(princ blockReferences)
(princ)

; (vla-put-Name 
	; (vla-item  (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*D45")
	; "myCrap"
; )

