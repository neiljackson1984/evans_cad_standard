; This script modifies the mleader style named nameOfDestinationStyle,
; creating it if it does not already exist, 
; so that all the properties of the destination style match all of the properties of the sourceStyle

(defun mleaderStyleClone (nameOfSourceStyle nameOfDestinationStyle /
    ; sourceStyle
    ; destinationStyle
)
    (setq sourceStyle 
        (vl-catch-all-apply 'vla-item 
            (list 
                (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_MLEADERSTYLE") 
                nameOfSourceStyle
            )
        )
    )
    (setq destinationStyle
        (vl-catch-all-apply 'vla-item 
            (list 
                (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_MLEADERSTYLE") 
                nameOfDestinationStyle
            )
        )
    )
    (if (vl-catch-all-error-p destinationStyle)
        (progn
            (setq destinationStyle
                (vla-AddObject
                    (vla-item (vla-get-Dictionaries (vla-get-ActiveDocument (vlax-get-acad-object))) "ACAD_MLEADERSTYLE")
                    nameOfDestinationStyle
                    "AcDbMLeaderStyle"
                )
            )
        )
    )
    ; We could probably get away with simply calling AddObject without bothering to check for the existence of a pre-existing destination style,
    ; because vla-Add overwrites automatically if there happens to be an existing object, and we are  completely specifying the destination style, so there is no need
    ; to preserve any information from the existing destination style.
    
    (mapcar
        '(lambda (propertyName) 
            (princ "now applying the property ")(princ propertyName)(princ ", whose value is ")(princ (vlax-get-property sourceStyle propertyName))(princ "\n")
            (if (vl-catch-all-error-p (vl-catch-all-apply 'vlax-put-property (list destinationStyle propertyName (vlax-get-property sourceStyle propertyName))))
                (progn 
                    (princ "failed\n")
                )
            )
            ; the Block and ArrowSymbol properties are expected to be the name of a block definition.  If we attempt to assign an empty string to either of 
            ; these properties, even though that is a perfectly valid value for the propreties to return, an exception will be thrown.  Hence the error checking above.
        )
        (list
            "AlignSpace"
            "Annotative"
            ; "Application"
            "ArrowSize"
            "ArrowSymbol"
            "BitFlags"
            "Block"
            "BlockColor"
            "BlockConnectionType"
            "BlockRotation"
            "BlockScale"
            "BreakSize"
            "ContentType"
            "Description"
            ; "Document"
            "DoglegLength"
            "DrawLeaderOrderType"
            "DrawMLeaderOrderType"
            "EnableBlockRotation"
            "EnableBlockScale"
            "EnableDogleg"
            "EnableFrameText"
            "EnableLanding"
            "FirstSegmentAngleConstraint"
            ; "Handle"
            ; "HasExtensionDictionary"
            "LandingGap"
            "LeaderLineColor"
            "LeaderLineType"
            "LeaderLineTypeId"
            "LeaderLineWeight"
            "MaxLeaderSegmentsPoints"
            ; "Name"
            ; "ObjectID"
            ; "ObjectName"
            ; "OverwritePropChanged"
            ; "OwnerID"
            "ScaleFactor"
            "SecondSegmentAngleConstraint"
            "TextAlignmentType"
            "TextAngleType"
            "TextAttachmentDirection"
            "TextBottomAttachmentType"
            "TextColor"
            "TextHeight"
            "TextLeftAttachmentType"
            "TextRightAttachmentType"
            "TextString"
            "TextStyle"
            "TextTopAttachmentType"
        )
    )
    (princ)
)

(princ "\n")
(princ)