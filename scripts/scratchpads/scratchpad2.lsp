(setq blockReferences (list ))

(vlax-for blockDefinition 
	(vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object)))


		
			; (princ "now looking at blockDefinition ")(princ (vla-get-Name blockDefinition))(princ "\n")
			(vlax-for entity blockDefinition
				(if 
					(= (vla-get-ObjectName entity) "AcDbBlockReference" )
					(progn
						; (princ "found a blockReference within ")(princ (vla-get-Name blockDefinition))(princ "\n")
						(if
							(vl-catch-all-error-p 
								(setq nameOfBlockReference 
									(vl-catch-all-apply 
										'vla-get-Name (list entity)
									) 
								)
							)
							(progn
								(princ "trying to get the name of the block reference through an exception: ")(princ (vl-catch-all-error-message nameOfBlockReference))(princ "\n")
								(princ "\t")(princ "name of blockDefinition containing the nameless blockReference: ")(princ (vla-get-Name blockDefinition))(princ "\n")
								(vla-delete entity)
							)
							(progn
								(if 
									(= (vla-get-Name entity)   "*D45")
									(progn
										(princ "found a reference ")(princ "(handle: ")(princ (vla-get-Handle entity))(princ ") to (the block definition named \"*D45\") within ")(princ (vla-get-Name blockDefinition))(princ "\n")
										(appendTo 'blockReferences entity)
									)
								)
							)
						)
					)
				)
			)
		

)

(princ blockReferences)


; (vla-put-Name 
	; (vla-item  (vla-get-Blocks (vla-get-ActiveDocument (vlax-get-acad-object))) "*D45")
	; "myCrap"
; )

