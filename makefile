projectRoot=.
acad-sheetset-to-pdf_executable:=${projectRoot}/braids/evans_cad_standard/acad-sheetset-to-pdf.exe
#the sheet set file is the first .dst file in the current folder.
sheetsetfile:=$(firstword $(wildcard ${projectRoot}/*.dst))
timestamp:=$(shell date +"%Y.%m.%d-%H%M")
outputpdffile:=${projectRoot}/delivered/${timestamp}/$(basename $(notdir ${sheetsetfile}))_${timestamp}.pdf
getFullyQualifiedWindowsStylePath=$(shell cygpath --windows --absolute "$(1)")

default: ${sheetsetfile} $(dir ${outputpdffile})
	cmd /c "$(call getFullyQualifiedWindowsStylePath,${acad-sheetset-to-pdf_executable})" --sheetsetfile="$(call getFullyQualifiedWindowsStylePath,${sheetsetfile})" --outputpdffile="$(call getFullyQualifiedWindowsStylePath,${outputpdffile})"

$(dir ${outputpdffile}): 
	mkdir --parents "$(dir ${outputpdffile})"
