#!/bin/bash

apiKey="AIzaSyAnUa8J0u4xN34SN023LXYd2yqn-L8_g7A"
siteAddress="700 avenue D #102, SNOHOMISH, WA"
curl \
    --get \
    --url "https://maps.googleapis.com/maps/api/staticmap" \
    --data-urlencode "key=${apiKey}" \
    --data-urlencode "format=png8" \
    --data-urlencode "center=${siteAddress}" \
    --data-urlencode "size=640x640" \
    --data-urlencode "scale=2" \
    --data-urlencode "style=feature:poi|visibility:off" \
    --data-urlencode "maptype=roadmap" \
    --data-urlencode "zoom=15" \
    > "vicinity_map.png"
    
curl \
    --get \
    --url "https://maps.googleapis.com/maps/api/staticmap" \
    --data-urlencode "key=${apiKey}" \
    --data-urlencode "format=png8" \
    --data-urlencode "center=${siteAddress}" \
    --data-urlencode "size=640x640" \
    --data-urlencode "scale=2" \
    --data-urlencode "maptype=satellite" \
    --data-urlencode "zoom=19" \
    > "aerial_plan.png"

curl \
    --get \
    --url "https://maps.googleapis.com/maps/api/staticmap" \
    --data-urlencode "key=${apiKey}" \
    --data-urlencode "format=png8" \
    --data-urlencode "center=${siteAddress}" \
    --data-urlencode "size=640x640" \
    --data-urlencode "scale=2" \
    --data-urlencode "style=feature:poi|visibility:off" \
    --data-urlencode "maptype=roadmap" \
    --data-urlencode "zoom=18" \
    > "site_plan_template.png"
    
    