2018/02/15

The package management system for Evans depends on having git and braid installed. 

To create a new Evans project, run makeNewProject.bat, passing a single parameter on the command line: the path of the new project folder that is to be created.

The preferred way to propagate text from the sheet set properties into the title block is to have attributes whose value contains field codes.
For instance, the default value for the xxx attribute of the title_block block is
"\pxsm1,qc;{\H0.375;%<\AcSm SheetSet.project_title>%
\H0.1875;%<\AcSm SheetSet.project_subtitle_line1>%
%<\AcSm SheetSet.project_subtitle_line2>%
%<\AcSm SheetSet.project_subtitle_line3>%
%<\AcSm SheetSet.project_subtitle_line4>%}"

When AutoCAD encounters a field whose resolved value is an empty string, AutoCAD does not display an empty string.  Rather, AutoCAD displays the string "----".
To force AutoCAD to display no printing characters for the resolved value of a field, we contrive to have the resolved value of the field be a string containing a single space. 
Unfortunately, the Sheetset manager has a tenedency to re-write any property values consisting of one or more spaces - the sheetset manager rewrites such a property value
as an empty string.  The work around is to set the value of a sheetset property to "\U+00A0".  This is the unicode escape sequence for a non-breaking spcace.
The sheetset manager does not rewrite such a property value, and the end result is that a field like %<\AcSm SheetSet.foo>% will resolve to a single nonbreaking space 
if the value of the sheetset property named foo is "\U+00A0".

How to generate the aerial plan and vicinity map:

(see https://developers.google.com/maps/documentation/maps-static/ for full details of the Google maps API).

siteAddress="12620 164th ave. se renton, wa"
curl \
    --get \
    --url "https://maps.googleapis.com/maps/api/staticmap" \
    --data-urlencode "format=png8" \
    --data-urlencode "center=${siteAddress}" \
    --data-urlencode "size=640x640" \
    --data-urlencode "scale=2" \
    --data-urlencode "style=feature:poi|visibility:off" \
    --data-urlencode "maptype=roadmap" \
    --data-urlencode "zoom=15" \
    > "vicinity_map.png"
    
curl \
    --get \
    --url "https://maps.googleapis.com/maps/api/staticmap" \
    --data-urlencode "format=png8" \
    --data-urlencode "center=${siteAddress}" \
    --data-urlencode "size=640x640" \
    --data-urlencode "scale=2" \
    --data-urlencode "maptype=satellite" \
    --data-urlencode "zoom=17" \
    > "aerial_plan.png"

The above will generate an image whose size is 1280*1280.
We will crop to 1280x814, taken from the center.  This will cut off the Google logo.

Sincerely,<br/>
Neil Jackson<br/>
neil@autoscaninc.command425-218-6726 (cell)<br/>
206-282-1616 ext. 102 (office)<br/>
Autoscan, Inc.<br/>
4040 23RD AVE W<br/>
SEATTLE WA 98199-1209<br/>
206-282-1616<br/>


